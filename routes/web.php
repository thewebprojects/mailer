<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/

Auth::routes();

//security issues
Route::redirect('/register', '/');
Route::redirect('/password/reset', '/');
Route::redirect('/password/email', '/');
//End Security issues


Route::get('/', 'WebsiteController@index')->name('index');

Route::post('/subscribe','WebsiteController@subscribe')->name('subscribe');
Route::post('/check-email-validate','WebsiteController@isSetEmail')->name('subscribe');


Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {
    Route::get('/','AdminController@index')->name('admin');
    Route::get('/players','AdminController@players')->name('players');

    Route::get('/subscribers','AdminController@subscribers')->name('subscribers');

    Route::resource('players', 'PlayerController');

    Route::get('/players/emails/edit/{player_espn_id?}', 'PlayerController@showEditEmail')->name('players.email.edit');
    Route::post('/players/emails/edit/', 'PlayerController@editEmail');
    
    Route::get('/players/weekly-data/update', 'PlayerController@updatePlayerWeklyData');
    Route::get('/players/all/search', 'PlayerController@searchPlayer');

    Route::get('/players/id/search', 'PlayerController@showPlayer')->name('search-player');

    Route::resource('subscribers', 'SubscribersController');
});


Route::get('/admin/trend-players-mail', 'MailController@sendTrendPlayersMail')->name('trend-players-mail');
Route::get('/scrap-pga/', 'MailController@scrapPgaData');



Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    
    return "Cache is cleared";
});

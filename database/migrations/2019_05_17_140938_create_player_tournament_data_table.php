<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerTournamentDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_tournament_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('player_espn_id', 9);
            $table->string('tournament_period');
            $table->string('tournament_name');
            $table->string('tournament_place');
            $table->string('player_position');
            $table->string('scores');
            $table->bigInteger('money_earned');

            $table->timestamps();

            $table->foreign('player_espn_id')->references('espn_id')
                ->on('players')
                ->onDelete('cascade')
                ->onUpdate('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_tournament_data', function (Blueprint $table) {
            $table->dropForeign(['player_espn_id']);
        });

        Schema::dropIfExists('player_tournament_data');
    }
}

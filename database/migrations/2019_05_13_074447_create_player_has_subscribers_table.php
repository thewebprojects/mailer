<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerHasSubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_has_subscribers', function (Blueprint $table) {
            $table->bigInteger('subscriber_id')->unsigned();
            $table->string('player_espn_id', 9);
            
            $table->foreign('player_espn_id')->references('espn_id')
                ->on('players')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('subscriber_id')->references('id')
                ->on('subscribers')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_has_subscribers', function (Blueprint $table) {
            $table->dropForeign(['subscriber_id']);
            $table->dropForeign(['player_espn_id']);
        });

        Schema::dropIfExists('player_has_subscribers');
    }
}

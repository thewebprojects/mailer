<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerTotalEarningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_total_earnings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('player_espn_id', 9);
            $table->bigInteger('earnings_in_one_year')->unsigned();
            $table->timestamps();

            $table->foreign('player_espn_id')->references('espn_id')
                ->on('players')
                ->onDelete('cascade')
                ->onUpdate('cascade');             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_total_earnings', function (Blueprint $table) {
            $table->dropForeign(['player_espn_id']);
        });
                
        Schema::dropIfExists('player_total_earnings');
    }
}

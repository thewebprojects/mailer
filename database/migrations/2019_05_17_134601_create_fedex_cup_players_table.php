<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFedexCupPlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fedex_cup_players', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('player_espn_id', 9);
            $table->timestamps();

            $table->foreign('player_espn_id')->references('espn_id')
                ->on('players')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fedex_cup_players', function (Blueprint $table) {
            $table->dropForeign(['player_espn_id']);
        });

        Schema::dropIfExists('fedex_cup_players');
    }
}

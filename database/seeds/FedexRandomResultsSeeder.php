<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Player;

class FedexRandomResultsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fedexRandomResultsData = [];

        $players = Player::all();


        foreach ($players as $player) {
            for ($i = 0; $i < 12; $i++) {
                $randomDate = Carbon::now()->subDays(rand(1, 1000));

                $fedexRandomResultsData[] = [
                    'player_espn_id' => $player->espn_id,
                    'fedex_rank' => rand(1, $players->count()),
                    'created_at' => $randomDate,
                ];
            }
        }

        DB::table('fedex_cup_results')->insert($fedexRandomResultsData);
    }
}

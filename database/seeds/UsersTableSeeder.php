<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (User::count() < 2) {
            Schema::disableForeignKeyConstraints();
            DB::table('users')->truncate();       
        }

        $adminUser = [
            'first_name' => 'John',
            'last_name' => 'Smith',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('11112222'),
        ];


        $user = User::create($adminUser);
    }
}

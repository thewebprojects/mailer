<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MondayMorningEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $players;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($players)
    {
        $this->players = $players;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $data = [
            'players' => $this->players,
        ];

        return $this->view('emails.monday-morning')->with($data);
    }
}

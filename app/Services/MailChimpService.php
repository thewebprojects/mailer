<?php

namespace App\Services;

use Log;

/**
 * MailChimp service
 */
class MailChimpService {
    /**
     * @var bool
     */
    private $success = true;

    /**
     * @var bool
     */
    private $message = "";

    /**
     * @var array
     */
    private $data = [];

    /**
     * MailChimpService constructor.
     * @param
     */
    public function __construct() {
        $this->debug = false;

        $this->clearResult();
    }

    /**
     * Add subscriber to MailChimp list
     *
     * @param $data
     * @return mixed
     */
    public function addSubscriber($data) {
        $this->clearResult();
        try {
            if($this->debug) {
                Log::info("MailChimp: start to add subscriber " . $data['email']);
            }

            if($data['type'] == 'freelancer'){
                $this->success = 0;
                $this->message = "User should be employer";
                return $this->getResult();
            }

            $firstName = $data['first_name'];
            $lastName = $data['last_name'];
            $email = $data['email'];
            $country = $data['country'];
            $hires = in_array($data['contract'], ['No contract', 'Pending', 'Active', 'End']) ? $data['contract'] : 'No contract';
            $payment = in_array($data['payment'], ['no Payment Method', 'Paypal', 'Arca']) ? $data['payment'] : 'no Payment Method';
            $createdAt = date("m/d/Y");

            // MailChimp API credentials
            $apiKey = env('MAILCHIMP_API_KEY');
            $listID = env('MAILCHIMP_LIST_ID');

            // MailChimp API URL
            $memberID = md5(strtolower($email));
            $dataCenter = substr($apiKey,strpos($apiKey,'-') + 1);
            $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;

            // member information
            $json = json_encode([
                'email_address' => $email,
                'status'        => 'subscribed',
                'merge_fields'  => [
                    'FNAME'     => $firstName . ' ' . $lastName,
                    'COUNTRY'   => $country ? $country : '',
                    'HIRES'     => $hires,
                    'PAYMENT'     => $payment,
                    'CREATEDAT'   => $createdAt,
                ]
            ]);

            // send a HTTP POST request with curl
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            $result = curl_exec($ch);
            //print_r($result);exit;
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            // store the status message based on response code
            if ($httpCode == 200) {
                $this->success = true;
                $this->message = "{$email} has successfully subscribed";
            } else {
                $this->success = false;
                $details = json_decode($result, true);
                if(isset($details['detail']) && $details['detail']){
                    $this->message = $details['detail'];
                }
                else {
                    switch ($httpCode) {
                        case 214:
                            $this->message = "{$email} is already subscribed.";
                            break;
                        default:
                            $this->message = "Some problem occurred, please try again.";
                            break;
                    }
                }
            }
            Log::info("MailChimp: " . $this->message);
        } catch (\Exception $e) {
            $this->success = false;
            $this->message = "Exception " . get_class($e) . ": " . $e->getMessage();

            if($this->debug) {
                Log::error($this->message);
            }
        }

        return $this->getResult();
    }

    private function clearResult(){
        $this->success = true;
        $this->message = "";
        $this->data = [];
    }

    private function getResult(){
        return [
            'success' => $this->success,
            'message' => $this->message,
            'data' => $this->data,
        ];
    }

    /**
     * Get MailChimp templates list
     *
     * @param null $id
     * @return array
     */
    public function getTemplates($id = null) {
        $this->clearResult();
        try {
            if($this->debug) {
                Log::info("MailChimp: start to get templates list");
            }

            // MailChimp API credentials
            $apiKey = env('MAILCHIMP_API_KEY');
            $listID = env('MAILCHIMP_LIST_ID');

            // MailChimp API URL
            $dataCenter = substr($apiKey,strpos($apiKey,'-') + 1);
            $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/templates' . ($id ? '/' . $id : '') . '?count=100&type=user';

            // send a HTTP GET request with curl
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            //print_r(json_decode($result, true));exit;
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            // store the status message based on response code
            if ($httpCode == 200) {
                $this->success = true;
                $this->message = $id ? "Template successfully returned" : "Templates list successfully returned";
                $this->data = json_decode($result, true);
            } else {
                $this->success = false;
                $details = json_decode($result, true);
                if(isset($details['detail']) && $details['detail']){
                    $this->message = $details['detail'];
                }
                else {
                    switch ($httpCode) {
                        default:
                            $this->message = "Some problem occurred, please try again.";
                            break;
                    }
                }
            }
            Log::info("MailChimp: " . $this->message);
        } catch (\Exception $e) {
            $this->success = false;
            $this->message = "Exception " . get_class($e) . ": " . $e->getMessage();

            if($this->debug) {
                Log::error($this->message);
            }
        }

        return $this->getResult();
    }
}

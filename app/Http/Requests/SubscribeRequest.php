<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubscribeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subscriber_email' => 'required|email|max:191|unique:subscribers,email',
            'player_espn_ids' => 'required|array|min:1'
        ];
    }


    public function attributes()
    {
        return [
            'subscriber_email' => 'email',
            'player_espn_ids' => 'player subscription'
        ];
    }
}

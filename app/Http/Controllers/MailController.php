<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MondayMorningEmail;
use App\Models\Player;
use App\Models\Subscriber;
use App\Models\FedexCupResults;
use PHPHtmlParser\Dom;

class MailController extends Controller
{
    public function sendSubscribedPlayersMail()
    {
        $subscribers = Subscriber::all(); 
        $subscribersData = [];
        
        $fedexPoints = FedexCupResults::all();

        foreach ($subscribers as $subscriber) {
            foreach($subscriber->getPlayers() as $player) {
                $player->subscriber_email = $subscriber->email;
                
                $subscribersData[] = [
                    'subscriber_email' => $subscriber->email,
                    'player' => $player,
                ];
            }
        }

        $subscribersData = collect($subscribersData)->groupBy('subscriber_email');


        foreach ($subscribersData as $subscriberEmail => $playersData) {
            $players = $playersData->pluck('player');
            
            Mail::to($subscriberEmail)->send(new MondayMorningEmail($players));
        }

        return response()->json(['success' => 200]);
    }


    public function sendTrendPlayersMail()
    {
        FedexCupResults::rankTrendDetect();
    }
}
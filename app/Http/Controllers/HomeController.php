<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Player;
use App\Models\Subscriber;
use Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $players = Players::get();

        // dd($players);
        return view('welcome',compact('players'));
    }

    public function subscribe(Request $request){
        $rules = array(
           'email' => 'required|email|max:191',
           'player_espn_id' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

//         dd($request->all());
// dd($validator->messages());
        if ($validator->fails()) {
           return redirect('/')
               ->withErrors($validator)->withInput();
        }

        $data = $request->all();
        Subscriber::create($data);
        return redirect('/')->with('success','Subscribtion created successfully!');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreSubscriberRequest;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Subscriber;
use App\Models\Player;

class SubscribersController extends Controller
{
    protected $paginationCount = 10;

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscribers = Subscriber::paginate($this->paginationCount);

        $data = [
            'subscribers' => $subscribers,
        ];

        return view('subscribers/index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $players = Player::all();

        $data = [
            'players' => $players,
        ];

        return view('subscribers/add')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubscriberRequest $request)
    {
        $data = $request->all();

        $subscriber = Subscriber::create([
            'email' => $request->subscriber_email,
        ]);

        // if ($subscriber instanceof Subscriber) {
        if ($subscriber instanceof Subscriber) {
            $data = [];

            foreach ($request->player_espn_ids as $playerId) {
                $data[] = [
                    'subscriber_id' => $subscriber->id,
                    'player_espn_id' => $playerId,
                ];
            }

            DB::table('player_has_subscribers')->insert($data);
            
            return back()->with('success','Subscribtion created successfully!');
        } else {
            return back()->with('error','Some problemms occured on server side!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

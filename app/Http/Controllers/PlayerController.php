<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Models\FedexCupPlayers;
use App\Models\Player;
use Carbon\Carbon;

class PlayerController extends Controller
{
    protected $paginationCount = 10;

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $players = Player::paginate($this->paginationCount);

        $data = [
            'players' => $players,
            'paginationCount' => $this->paginationCount,
            'page' => $request->page ?? 0,
        ];


        return view('players/index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('players/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
           'first_name' => 'required|max:191',
           'last_name' => 'required|max:191',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
           return redirect('/players/create')
               ->withErrors($validator)->withInput();
        }

        $data = $request->all();
        
        Player::create($data);
        return redirect('/admin/players')->with('success','Player created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function showEditEmail($playerId)
    {   
        $player = Player::where('espn_id', $playerId)->get()->first();
        
        $data = [
            'player' => $player,
        ];

        return view('players.edit-email')->with($data);
    }

    public function editEmail(Request $request)
    {
        try {
            $result = Player::where('espn_id', $request->player_espn_id)->update(
                [
                    'email_text' => $request->email_text
            ]);

            return back()->with('success', 'Email text edited successfully!');
        } catch(\Throwable  $e) {
            return back()->with('error', 'Email text edit failed!');
        }
    }


    public function updatePlayerWeklyData()
    {
        \Artisan::call('players-data:update');
    }

    public function searchPlayer(Request $request)
    {
        $q = $request->q;

        $players = Player::where('first_name', 'like', "%$q%")
                        ->orWhere('last_name', 'like', "%$q%")
                        ->take(6)->get();

        $data = [];

        if ($players->isNotEmpty()) {
            foreach ($players as $player) {
                $data[] = [
                    'espn_id' => $player->espn_id,
                    'full_name' => $player->first_name . ' ' . $player->last_name,
                ];
            }
        }

        return response()->json($data);
    }

    public function showPlayer(Request $request)
    {
        $players = Player::paginate($this->paginationCount);
        $players = Player::where('espn_id', $request->espn_id)->paginate($this->paginationCount);

        $data = [
            'players' => $players,
            'paginationCount' => $this->paginationCount,
            'page' => $request->page ?? 0,
        ];


        return view('players/index')->with($data);        
    }
}

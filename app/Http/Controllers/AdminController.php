<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \DrewM\MailChimp\MailChimp;
use App\Services\MailChimpService;
use App\Models\Player;
use App\Models\Subscriber;

class AdminController extends Controller
{

    protected $paginationCount = 10;
    /**
    * @var MailChimpService
    */
    private $mailChimpService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MailChimpService $mailChimpService)
    {
        $this->mailChimpService = $mailChimpService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        /*$mailChimpRes = $this->mailChimpService->getTemplates();

        $templates = $mailChimpRes['success'] ? (array)$mailChimpRes['data']['templates'] : [];

        $MailChimp = new MailChimp(env('MAILCHIMP_API_KEY'));
        dd($mailChimpRes);
        $result = $MailChimp->get('lists');

        // dd($result);
        $list_id = '31868aa788';
        $result = $MailChimp->post("lists/$list_id/members", [
  				'email_address' => 'sargsyanshahen@gmail.com',
  				'status'        => 'subscribed',
  			]);

        dd($result);*/


        return view('admin.index');
    }

    public function players()
    {   
        $players = Player::paginate($this->paginationCount);

        $data = [
            'players' => $players,
            'paginationCount' => $this->paginationCount,
        ];
        
        return view('players/index')->with($data);
    }


    public function subscribers()
    {
        $subscribers = Subscriber::paginate($this->paginationCount);

        $data = [
            'subscribers' => $subscribers
        ];

        return view('subscribers/index')->with($data);
    }
}

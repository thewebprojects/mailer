<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SubscribeRequest;
use Illuminate\Support\Facades\DB;
use App\Models\Player;
use App\Models\PlayerHasSubscriber;
use App\Models\Subscriber;
use Illuminate\View\View;
use Psy\Util\Json;

class WebsiteController extends Controller
{
    public function index()
    {
        $players = Player::all()->take(20);

        $player = $players->where('espn_id', '10050')->first();


//         foreach ($players as $player) {
//             dump($player->tournaments);
//         }

        $data = [
            'players' => $players,
            'player' => $player,
        ];

        return view('welcome')->with($data);//TODO with hasMany
    }

    public function subscribe(SubscribeRequest $request)
    {   
        $subscriber = Subscriber::create([
            'email' => $request->subscriber_email,
        ]);

        if ($subscriber instanceof Subscriber) {
            $data = [];
            foreach ($request->player_espn_ids as $playerEspnId) {
                $data[] = [
                    'subscriber_id' => $subscriber->id,
                    'player_espn_id' => $playerEspnId,
                ];
            }

            try {
                DB::table('player_has_subscribers')->insert($data);
            } catch(\Throwable  $e) {
                $subscriber->truncate();
            }
            
            return redirect('/')->with('success','Subscription created successfully!');
        }
    }
    public function isSetEmail(Request $request){
        $subscriber = Subscriber::where('email', $request->input('email'))->count();
        if ($subscriber>0){
            return View::make('ajax.updateSubscribers');
            //TODO stexic shatunakel
        }
        else{
            return false;
        }

    }
}

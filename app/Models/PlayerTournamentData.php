<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlayerTournamentData extends Model
{
    public $table = 'player_tournament_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $guarded = [];

    public function earning()
    {
        $earning = number_format((int)$this->money_earned);
    
        $earning = '$' . $earning;


        return $earning;      
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FedexCupPlayers extends Model
{
    public $table = 'fedex_cup_players';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $guarded = [];
   
}

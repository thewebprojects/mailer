<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    public $timestamps = false;
    /**
     * @var string
     */
    public $table = 'subscribers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'player_espn_id','email', 'status',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
    */
    public function playerData()
    {
        return $this->hasMany('App\Models\PlayerHasSubscriber', 'subscriber_id');
    }

    public function getPlayers()
    {
        $players  = $this->playerData->map(function($item, $index) {
            return $item->player;
        });

        return $players;
    }
}

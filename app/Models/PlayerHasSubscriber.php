<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlayerHasSubscriber extends Model
{
    public $timestamps = false;
    /**
     * @var string
     */
    public $table = 'player_has_subscribers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $guarded = [];

    public function player()
    {
        return $this->belongsTo('App\Models\Player', 'player_espn_id', 'espn_id');        
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlayerTotalEarning extends Model
{
    public $table = 'player_total_earnings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $guarded = [];
}

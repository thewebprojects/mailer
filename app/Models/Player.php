<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use ZingChart\PHPWrapper\ZC;

class Player extends Model
{
    public $timestamps = false;
    /**
     * @var string
     */
    public $table = 'players';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name',
    ];


    public static function fedexCupPlayers()
    {
        $players = Player::all();

        $data = collect([]);

        foreach ($players as $player) {
            if($player->isPlayInFedexCup()) {
                $data->push($player);
            }
        }

        return $data;
    }

    public function fedexData()
    {
        return $this->hasOne('App\Models\FedexCupPlayers', 'player_espn_id', 'espn_id');      
    }

    public function fedexResulst()
    {
        return $this->hasMany('App\Models\FedexCupResults', 'player_espn_id', 'espn_id');      
    }

    public function isPlayInFedexCup()
    {

        if($this->fedexData === null) {
            return false;
        } else {
            return true;
        }
    }

    public function generateEspnPlayerUrl()
    {
        $espnURL = 'http://www.espn.com/golf/player/results/_/id/' . $this->espn_id;

        return $espnURL;
    }

    public function tournaments()
    {
        return $this->hasMany('App\Models\PlayerTournamentData', 'player_espn_id', 'espn_id');
    }


    public function earnings()
    {
        return $this->hasMany('App\Models\PlayerTotalEarning', 'player_espn_id', 'espn_id')->orderBy('created_at', 'desc');
    }

    public function fullName()
    {
        return $this->first_name . '&nbsp;&nbsp;&nbsp;' . $this->last_name;
    }


    public function totalEarning()
    {   
        $earning = number_format((int)$this->earnings()->first()->earnings_in_one_year);
    
        $earning = '$' . $earning;


        return $earning;
    }
}

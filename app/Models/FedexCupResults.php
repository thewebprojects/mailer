<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FedexCupResults extends Model
{
    public $table = 'fedex_cup_results';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $guarded = [];


    public static function rankTrendDetect()
    {
        $rankResults = FedexCupResults::all();

        $rankResults = $rankResults->groupBy('player_espn_id');

        dd($rankResults['10140']);
    }
}

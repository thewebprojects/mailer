<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlayerWeeklyData extends Model
{
    public $table = 'player_weekly_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $guarded = [];
}

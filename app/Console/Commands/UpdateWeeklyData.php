<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\FedexCupPlayers;
use App\Models\Player;
use Carbon\Carbon;
use DiDom\Document;

class UpdateWeeklyData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'players-data:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update players weekly data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);
        
        $fedexCupPlayerIds = FedexCupPlayers::all()->pluck('player_espn_id');
        $players = Player::all()->take(20);
        // $players = Player::all()->whereNotIn('espn_id', ['3670']);
        // $players = Player::all()->where('espn_id', '257');

        $fedexData = [];
        $playerTournamentData = [];
        $playerTotalEarningsData = [];


        $createDate = Carbon::now();

        //This part must be dynamic in future
        $fedExURL = 'http://www.espn.com/golf/statistics/_/sort/cupPoints/count/';
        $urlPagination = 40;
        // $totalFedexPlayers = FedexCupPlayers::count();
        $totalFedexPlayers = 252;
        $counter = 0;

        $fedExDomData = [];

        while ($totalFedexPlayers - $urlPagination * $counter > 0) {
            $urlGetParametr = $urlPagination * $counter === 0 ? 1 : $urlPagination * $counter;

            $document = new Document($fedExURL . $urlGetParametr, true);

            $fedExDomData[] = $document;

            $counter++;
        }
        //----------------


        // try {
            $lastNotZeroFedexRank = 1;

            foreach ($players as $player) {
                $espnUrl = $player->generateEspnPlayerUrl();
                $document = new Document($espnUrl, true);


                if (isset($document->find('body .header-stats')[1])) {
                    $playerOneYearEarningDOM = $document->find('body .header-stats')[1]->find('tr');

                    $playerOneYearEarning = str_replace(',', '', $playerOneYearEarningDOM[0]->lastChild()->innerHTML());

                    $playerOneYearEarning = str_replace('$', '', $playerOneYearEarning);
                } else {
                    $playerOneYearEarning = 0;
                }


                $playerTotalEarningsData[] = [
                    'player_espn_id' => $player->espn_id,
                    'earnings_in_one_year' => (int)$playerOneYearEarning,
                    'created_at' => $createDate,
                ];


                if (isset($document->find('.mod-content')[2])) {
                    $tournamentTable = $document->find('.mod-content')[2];

                    $firstTournament = $tournamentTable->find('tr')[2];
                    
                    $firstTournamentData = $firstTournament->find('td');

                    $tournamentPeriod = trim($firstTournamentData[0]->innerHTML());
                    $tournamentName = trim($firstTournamentData[1]->find('a')[0]->innerHTML());
                    $tournamentPlace = trim($firstTournamentData[1]->find('i')[0]->innerHTML());
                    $playerPosition = trim($firstTournamentData[2]->innerHTML());
                    $tournamentScore = trim($firstTournamentData[3]->find('b')[0]->innerHTML());

                    if (isset($firstTournamentData[4])) {
                        $playerEarned = str_replace([',', '$'], ['', ''], $firstTournamentData[4]->innerHTML);
                    } else {
                        $playerEarned = 0;
                    }

                    $playerTournamentData[] = [
                        'player_espn_id' => $player->espn_id,
                        'tournament_period' => $tournamentPeriod,
                        'tournament_name' => $tournamentName,
                        'tournament_place' => str_replace(['<br>', '<br/>', '<br />'], ['', '', ''] , $tournamentPlace),
                        'player_position' => $playerPosition,
                        'scores' => $tournamentScore,
                        'money_earned' => (int)$playerEarned,
                        'created_at' => $createDate,
                    ];
                }


                foreach ($fedExDomData as $fedexDom) {
                    if (! empty($fedexDom->find('.player-1106-' . $player->espn_id))) {
                        $playerRankDOM = $fedexDom->find('.player-1106-' . $player->espn_id)[0];

                        $fedexRank = (int)trim($playerRankDOM->firstChild()->text());
                        
                        //it can be more accurate if it will be retrieved from dom
                        if ($fedexRank === 0) {
                            $fedexRank = $lastNotZeroFedexRank;
                        } else {
                            $lastNotZeroFedexRank = $fedexRank;
                        }

                        $fedexData = [
                            'player_espn_id' => $player->espn_id,
                            'fedex_rank' => $fedexRank,
                            'created_at' => $createDate,
                        ];                            
                    }
                }
            }

            if (! empty($fedexData)) {
                DB::table('fedex_cup_results')->insert($fedexData);
            }
            
            DB::table('player_total_earnings')->truncate();
            DB::table('player_total_earnings')->insert($playerTotalEarningsData);


            DB::table('player_tournament_data')->truncate();
            DB::table('player_tournament_data')->insert($playerTournamentData);

        // } catch(\Throwable  $e) {
        //     dd($e->getMessage());
        // }
    }
}

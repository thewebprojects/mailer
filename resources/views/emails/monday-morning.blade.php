<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>AdminDesigns Email Template - Invite</title>

    <style type="text/css">
      .tournament-table th {
        border: solid 1px; padding: 0px 15px 0px 15px;
        text-align: center;
      }

      .tournament-table td {
        border: solid 1px; padding: 0px 15px 0px 15px;
        text-align: center;
      }

      .tournament-table {
        margin-left: auto;
        margin-right: auto;
      }

      .player-text {
        width: 800px;
        background-color: #f3f3f3;
        margin-top: 10px;
        margin-bottom: 10px;
        margin-left: auto;
        margin-right: auto;
        padding-left: 20px;
        text-align: justify;
      }
    </style>
</head>

<body bgcolor="#f7f7f7">
    @foreach($players as $player)
      <h1>{!! $player->fullName() !!}</h1>
      <div>
        <table class="tournament-table" width="100%">
          <thead>
            <tr>
              <th>DATE</th>
              <th>TOURNAMENT</th>
              <th>SCORE</th>
              <th>POS</th>
              <th>TOURNAMENT EARNINGS</th>
              <th>TOTAL EARNINGS</th>
            </tr>
          </thead>

          <tbody>
            @foreach($player->tournaments as $tournamentData)
              <tr>
                <td>{{ $tournamentData->tournament_period }}</td>
                <td>
                  {{ $tournamentData->tournament_name }}
                  <div><i>{{ $tournamentData->tournament_place }}</i></div>
                </td>
                <td>{{ $tournamentData->player_position }}</td>
                <td>{{ $tournamentData->scores }}</td>
                <td>{{ $tournamentData->earning() }}</td>
                <td>{{ $player->totalEarning() }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>

        <div class="player-text">
          {{ $player->email_text }}
        </div>
      </div>
    @endforeach
</body>
</html>

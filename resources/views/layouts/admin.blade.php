<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>Mailer</title>
  <meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme" />
  <meta name="description" content="Mailer">
  <meta name="author" content="Mailer">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="base-url" content="{{ URL::to('/') }}">
  <!-- Font CSS (Via CDN) -->


  <!-- Theme CSS -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="{{ asset('css/theme.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/admin-forms.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/footable.core.min.css') }}">

  <!-- Favicon -->
  <link rel="shortcut icon" href="{{ asset('/img/favicon.ico')}}">

  @stack('header-links')
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>

<body class="">
  <!-- Start: Main -->
  <div id="main">

    <!-- Start: Header -->
    <header class="navbar navbar-fixed-top">
      <div class="navbar-branding">
        <a class="navbar-brand" href="{{ URL::to('/') }}">
          Visit <b>Mailer</b>
            <img src="{{ asset('img/avatars/profile_avatar.jpg')}}" alt="avatar" class="mw30 br64 mr15"> John.Smith
        </a>
        <span id="toggle_sidemenu_l" class="ad ad-lines"></span>
      </div>

      <!-- <form class="navbar-form navbar-left navbar-search" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search..." value="Search...">
        </div>
      </form> -->

      <ul class="nav navbar-nav navbar-right">


        <li class="menu-divider hidden-xs">
          <i class="fa fa-circle"></i>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle fw600 p15" data-toggle="dropdown">
              <img src="{{ asset('img/avatars/profile_avatar.jpg')}}" alt="avatar" class="mw30 br64 mr15"> John.Smith
            <span class="caret caret-tp hidden-xs"></span>
          </a>
          <ul class="dropdown-menu list-group dropdown-persist w250" role="menu">


            <li class="list-group-item">
              <a href="{{url('user/edit')}}" class="animated animated-short fadeInUp">
                <span class="fa fa-gear"></span> Account Settings </a>
            </li>
            <li class="list-group-item">
              <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="animated animated-short fadeInUp">
                <span class="fa fa-power-off"></span> Logout </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
          </ul>
        </li>
      </ul>

    </header>
    <!-- End: Header -->

    <!-- Start: Sidebar -->
    <aside id="sidebar_left" class="nano nano-primary affix">

      <!-- Start: Sidebar Left Content -->
      <div class="sidebar-left-content nano-content">

        <!-- Start: Sidebar Header -->
        <header class="sidebar-header">

          <!-- Sidebar Widget - Author (hidden)  -->
          <div class="sidebar-widget author-widget hidden">
            <div class="media">
              <a class="media-left" href="#">
                <img src="{{ asset('img/avatars/3.jpg') }}" class="img-responsive">
              </a>
              <div class="media-body">
                <div class="media-links">
                   <a href="{{ route('logout') }}" class="sidebar-menu-toggle">User Menu -</a> <a href="pages_login(alt).html">Logout</a>
                </div>
                <div class="media-author">Michael Richards</div>
              </div>
            </div>
          </div>
        </header>
        <!-- End: Sidebar Header -->

        <!-- Start: Sidebar Menu -->
        <ul class="nav sidebar-menu">
          <li class="sidebar-label pt20">Menu</li>
          @if(Auth::check())
            <li  class="{{ Request::is('dashboard') ? 'active' : '' }}">
                <a href="{{url('/admin')}}">
                    <span class="glyphicon glyphicon-book"></span>
                    <span class="sidebar-title">Dashboard</span>
                </a>
            </li>

            <li  class="{{ Request::is('subscribers')? 'active' : '' }}">
                <a href="{{url('/admin/subscribers')}}">
                    <span class="glyphicon glyphicon-check"></span>
                    <span class="sidebar-title">Subscriber</span>
                </a>
            </li>
            <li  class="{{ Request::is('players')? 'active' : '' }}">
                <a href="{{url('/admin/players')}}">
                    <span class="glyphicon glyphicon-check"></span>
                    <span class="sidebar-title">Players</span>
                </a>
            </li>

            <li>
                <a href="{{ url('/admin/players/weekly-data/update') }}">
                    <span class="glyphicon glyphicon-check"></span>
                    <span class="sidebar-title">Update data</span>
                </a>
            </li>
            
            {{-- FOR Developers --}}
            @if(URL::to('/') === 'http://mailer.loc')
              <li>
                  <a href="{{ route('trend-players-mail') }}">
                      <span class="glyphicon glyphicon-check"></span>
                      <span class="sidebar-title">Send trending email</span>
                  </a>
              </li>
            @endif
           @endif
        </ul>
        <!-- End: Sidebar Menu -->

      </div>
      <!-- End: Sidebar Left Content -->

    </aside>

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-check pr10"></i>
          {{ $message }}
        </div>
      @endif
      @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-remove pr10"></i>
          {{ $message }}
        </div>
      @endif

        @yield('content')
    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->

  <!-- Theme Javascript -->
  <script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
  <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
  <script src="{{ asset('js/moment.min.js') }}"></script>
  <script src="{{ asset('js/utility.js') }}"></script>
  <script src="{{ asset('js/demo.js') }}"></script>
  <script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
  <script src="{{ asset('js/jquery.bootstrap-duallistbox.min.js') }}"></script>
  <script src="{{ asset('js/footable.all.min.js') }}"></script>
  <script src="{{ asset('js/footable.filter.min.js') }}"></script>
  <script src="{{ asset('js/main.js') }}"></script>
  <script type="text/javascript">
    jQuery(document).ready(function() {

      "use strict";

      // Init Theme Core
      Core.init();
    });
  </script>
  <!-- END: PAGE SCRIPTS -->
    @yield('page-js-script')
    @stack('footer-scripts')
</body>

</html>

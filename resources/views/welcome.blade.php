@extends('layouts.website')

@section('content')
    <div class="container center_div col-xs-1 text-center vcenter">
        <form class="form-inline center_div" style="margin: 0 auto;width:100%">
            <div class="container center_div">
                <label for="inputPassword2" class="sr-only">Write Email</label>
                <input type="email" class="form-control" id="subscriber-email" placeholder="Email">

                <button type="button" class="btn btn-primary opensubscribemodal">Subscribe</button>
                <span class="text-danger d-block"><strong id="errorMassage"></strong></span>
                @if($errors->isNotEmpty())
                    <span class="text-danger d-block"><strong>{{ $errors->first() }}</strong></span>
                @endif
            </div>
            <!-- <button type="submit" class="btn btn-primary mb-2">Subscribe</button> -->
        </form>
    </div>
    </div>


    <div class="modal fade p-4" id="subscribe-player-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">Subscribe to players</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div id="modal-body">
                    <form  class="form-horizontal row" role="form" method="POST" action="{{ route('subscribe') }}" autocomplete="off">
                        @csrf
                        @foreach($players as $player)
                            <div class="form-row col-12 justify-content-center">
                                <label class="col-5" for="playerId{{ $player->espn_id }}">{{ $player->first_name }} {{ $player->last_name }}</label>
                                {{-- <input class="col-6 form-control" type="email" name="email" id="playerId{{ $player->espn_id }}"> --}}

                                <div class="form-check form-check-inline col-2">
                                    <input class="form-check-input" type="checkbox" id="player{{ $player->espn_id }}" name="player_espn_ids[]" value="{{ $player->espn_id }}">

                                    <label class="form-check-label" for="player{{ $player->espn_id }}"></label>
                                </div>
                            </div>
                        @endforeach

                        <input type="hidden" id="subscriber-modal-email" name="subscriber_email">

                        <div class="col-12 text-right mb-3">
                            <button type="submit" class="btn btn-primary btn-sm float-right mr-3">Subscribe</button>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('page-js-script')
    <script type="text/javascript">
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
        $(document).ready(function(){


            $(".opensubscribemodal").click(function(){
                var email = $("#subscriber-email").val();
                var valid = validateEmail(email);
                if(valid){
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url:'check-email-validate',
                        method:'POST',
                        data:{'email':email},
                        success:response=>{
                            if (response){
                                $("#modal-body").html(response);
                            }
                        },
                        error:e=>{
                            console.log(e);
                        }
                    });
                    $("#subscribe-player-modal").modal("show");
                    $("#subscriber-modal-email").val(email);
                }else {
                    $("#errorMassage").html('Please write correct email!');
                    return false;
                }



            });
        });
    </script>
@stop

@extends('layouts.admin')

@push('header-links')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-select.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/custom-bootstrap-select.css') }}">
@endpush

@section('content')
<header id="topbar">
  <div class="topbar-left">
    <ol class="breadcrumb">
      <li class="crumb-active">
        <a href="header-links">Dashboard</a>
      </li>
      <li class="crumb-icon">
        <a href="/">
          <span class="glyphicon glyphicon-home"></span>
        </a>
      </li>
      <li class="crumb-link">
        <a>Players</a>
      </li>
      <!--<li class="crumb-trail">Dashboard</li>-->
    </ol>
  </div>

</header>

<section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-center -->
    <div class="tray tray-center">

        <div class="panel mb25 mt5">
            <div class="panel-body p25 pb5">
              <div class="tab-content pn br-n admin-form">
                  <div class="section row mbn">
                    <div class="col-sm-8">
                      <label class="field option mt10">
                        <!--<input type="checkbox" name="info" checked>-->
                        <span class=""></span>Add Player
                      </label>
                    </div>
                    <div class="col-sm-4">
                      <p class="text-right">
                          <a href="{{url('/admin/players/create')}}" class="btn btn-primary" type="button">Add Player</a>
                      </p>
                    </div>
                  </div>
                  <!-- end section -->
              </div>
            </div>
          </div>

          <div class="mb15" style="width: 20%; margin-left: auto">
            <form id="select-player-form-id" method="GET" action="{{ route('search-player') }}">
              <select type="text" name="espn_id" id="search-player-input" data-live-search="true" title="search">
              </select>
            </form>
          </div>

          <div class="panel">
              <div class="panel-body pn">
                <div class="table-responsive">
                  <table class="table admin-form theme-warning tc-checkbox-1 fs13">
                    <thead>
                      <tr class="bg-light">
                        <th class="text-center">#</th>
                        <th class="">First Name</th>
                        <th class="">Last Name</th>
                        <th class="">Text to mail</th>
                      </tr>
                    </thead>

                    <tbody>
                      @foreach($players as $player)
                          <tr>
                              <td>{{ $loop->iteration + $paginationCount * $page}}</td>
                              <td>{{ $player->first_name }}</td>
                              <td>{{ $player->last_name }}</td>
                              <td>
                                <a href="{{ route('players.email.edit', ['player_espn_id' => $player->espn_id]) }}" class="btn-primary btn btn-sm">Edit</a>
                              </td>
                          </tr>
                      @endforeach
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="panel-footer clearfix">
                      {{ $players->links() }}
              </div>
          </div>
    </div>
</section>

@endsection

@section('page-js-script')
  <script src="{{ asset('js/ajax-bootstrap-select.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>


  <script type="text/javascript">
    $("#search-player-input").selectpicker({
      style : '',
      styleBase : 'form-control'
    }).ajaxSelectPicker({
      locale: {
        emptyTitle: " "
      },
      ajax: {
        url: $('meta[name="base-url"]').attr('content') + '/admin/players/all/search', 
        type: 'GET',
        dataType: 'json',

        data: {
          search_text: '@{{{q}}}'
        }
      },
      // function to preprocess JSON data
      preprocessData: function (data) {
        var i, l = data.length, array = [];
        if (l) {
            for (i = 0; i < l; i++) {
                array.push($.extend(true, data[i], {
                    text : data[i].full_name,
                    value: data[i].espn_id,
                    // data : {
                    //     subtext: 'test'
                    // }
                }));
            }
        }
        // You must always return a valid array when processing data. The
        // data argument passed is a clone and cannot be modified directly.
        return array;
      }
    });


    $("#search-player-input").on('changed.bs.select', function() {
      $('#select-player-form-id').submit();
    });
  </script>
@stop

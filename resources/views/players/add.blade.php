@extends('layouts.admin')

@section('content')
<!-- Start: Topbar -->
<header id="topbar">
  <div class="topbar-left">
    <ol class="breadcrumb">
      <li class="crumb-active">
        <a href="/">Dashboard</a>
      </li>
      <li class="crumb-icon">
        <a href="/">
          <span class="glyphicon glyphicon-home"></span>
        </a>
      </li>
      <li class="crumb-link">
        <a>{{ Route::currentRouteNamed('players.create') ? 'Add' : 'Edit' }} Project</a>
      </li>
      <!--<li class="crumb-trail">Dashboard</li>-->
    </ol>
  </div>

</header>
<!-- End: Topbar -->
<section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

          <div class="mw1000 center-block">

                    <div class="tray tray-center">

                    <!-- Validation Example -->
                    <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
                        <div class="panel">
                            <div class="panel-heading">
                              <span class="panel-title">Add Player</span>
                            </div>
                            <div class="panel-body">
                                <form id="admin-form" class="form-horizontal" role="form" method="POST" action="{{ Route::currentRouteNamed('players.create') ? route('players.store') : route('players.update', ['id' => $data->id]) }}" autocomplete="off">
                                <input type="hidden" name="id" value="{{ $edit_id ?? '' }}">
                                <input type="hidden" name="_method" value="{{ Route::currentRouteNamed('players.create') ? 'POST' : 'PUT' }}">
                                {{ csrf_field() }}


                                <div class="form-group">
                                  <label for="name" class="col-lg-3 control-label">First Name</label>
                                  <div class="col-lg-8">
                                    <label for="name" class="field {{ $errors->has('first_name') ? 'state-error' : ''}}">
                                        <input type="text" value="{{ !is_null(old('first_name')) ? old('first_name') : (isset($data) ? $data->first_name : '') }}" name="first_name"  class="gui-input" placeholder="First Name">
                                    </label>
                                      @if($errors->has('first_name'))
                                      <em for="name" class="state-error">Enter first name</em>
                                      @endif
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label for="name" class="col-lg-3 control-label">Last Name</label>
                                  <div class="col-lg-8">
                                    <label for="name" class="field {{ $errors->has('last_name') ? 'state-error' : ''}}">
                                        <input type="text" value="{{ !is_null(old('last_name')) ? old('last_name') : (isset($data) ? $data->last_name : '') }}" name="last_name" class="gui-input" placeholder="Last Name">
                                    </label>
                                      @if($errors->has('last_name'))
                                      <em for="last_name" class="state-error">Enter last name</em>
                                      @endif
                                  </div>
                                </div>

                                <div class=" text-right">
                                    <button type="submit" class="button btn-primary"> Validate Form </button>
                                    <a href="/players" type="reset" class="button"> Cancel </a>
                                </div>
                              </form>
                            </div>
                        </div>
                    </div>
                    <!-- end: .admin-form -->
                </div>
                <!-- Begin: Admin Form -->
            </div>
        </div>
        <!-- end: .tray-center -->

</section>


@endsection

@extends('layouts.admin')

@section('content')
<!-- Start: Topbar -->

@include('layouts.includes.admin.header-breadcrumb')

<!-- End: Topbar -->
<section id="content" class="table-layout animated fadeIn">
  <!-- Request Google Analytics -->
  <div class="panel panel-system panel-border top mb35">
    <div class="panel-heading">
      <span class="panel-title">Edit Email</span>
    </div>
    <div class="panel-body bg-light dark">
      <div class="admin-form">

        {{-- <h5 class="text-muted fw400 mb10"> Edit email</h5> --}}
        <form  method="POST" action="{{ route('players.email.edit') }}">
          @csrf
          <div class="section mbn">
            <label class="field prepend-icon">
              <textarea class="gui-textarea" id="email-text" name="email_text" placeholder="Email text here">{{ $player->email_text }}</textarea>
              <label for="email-text" class="field-icon"></label>
            </label>

            <div class="text-right mt15">
              <button class="btn btn-sm btn-primary">submit</button>
            </div>
          </div>
          <input type="hidden" name="player_espn_id" value="{{ $player->espn_id }}">
        </form>
        <!-- end section -->

      </div>
    </div>
  </div>
</section>


@endsection

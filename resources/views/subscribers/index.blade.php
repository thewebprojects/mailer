@extends('layouts.admin')

@section('content')
<header id="topbar">
  <div class="topbar-left">
    <ol class="breadcrumb">
      <li class="crumb-active">
        <a href="">Dashboard</a>
      </li>
      <li class="crumb-icon">
        <a href="/">
          <span class="glyphicon glyphicon-home"></span>
        </a>
      </li>
      <li class="crumb-link">
        <a>Subscriber</a>
      </li>
      <!--<li class="crumb-trail">Dashboard</li>-->
    </ol>
  </div>

</header>

<section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-center -->
    <div class="tray tray-center">

        <div class="panel mb25 mt5">
            <div class="panel-body p25 pb5">
              <div class="tab-content pn br-n admin-form">
                  <div class="section row mbn">
                    <div class="col-sm-8">
                      <label class="field option mt10">
                        <!--<input type="checkbox" name="info" checked>-->
                        <span class=""></span>Add Subscriber
                      </label>
                    </div>
                    <div class="col-sm-4">
                      <p class="text-right">
                          <a href="{{ url('/admin/subscribers/create') }}" class="btn btn-primary" type="button">Add Subscriber</a>
                      </p>
                    </div>
                  </div>
                  <!-- end section -->
              </div>
            </div>
          </div>

          <div class="panel">
              <div class="panel-body pn">
                <div class="table-responsive">
                  <table class="table admin-form theme-warning tc-checkbox-1 fs13">
                    <thead>
                      <tr class="bg-light">
                        <th class="text-center">#</th>
                        <th class="">Subscriber Email</th>
                        <th class="">Subscribed to Player</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($subscribers as $subscriber)
                      <tr>
                          <td>{{ $subscriber->id }}</td>
                          <td>{{ $subscriber->email }}</td>
                          <td>
                            @if($subscriber->getPlayers()->isNotEmpty())
                              @foreach($subscriber->getPlayers() as $player)
                                <div><strong>({{ $loop->iteration }})&nbsp;&nbsp;&nbsp;</strong>{!! $player->fullName() !!}</div>
                              @endforeach
                            @endif
                          </td>
                      </tr>
                      @endforeach
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div class="panel-footer clearfix">
                  {{ $subscribers->links() }}
                  <a href="#" class="btn btn-success" style="float: right" role="button">Send Emails</a>
              </div>
          </div>
    </div>
</section>

@endsection

@section('page-js-script')
<script type="text/javascript">

</script>
@stop

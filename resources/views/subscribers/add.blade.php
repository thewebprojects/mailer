@extends('layouts.admin')
@push('header-links')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-select.min.css') }}">
@endpush
@section('content')
<!-- Start: Topbar -->
<header id="topbar">
  <div class="topbar-left">
    <ol class="breadcrumb">
      <li class="crumb-active">
        <a href="/">Dashboard</a>
      </li>
      <li class="crumb-icon">
        <a href="/">
          <span class="glyphicon glyphicon-home"></span>
        </a>
      </li>
      <li class="crumb-link">
        <a>{{ Route::currentRouteNamed('subscribers.create') ? 'Add' : 'Edit' }} Subscriber</a>
      </li>
      <!--<li class="crumb-trail">Dashboard</li>-->
    </ol>
  </div>

</header>
<!-- End: Topbar -->
<section id="content" class="table-layout animated fadeIn">
        <!-- begin: .tray-center -->
        <div class="tray tray-center">

          <div class="mw1000 center-block">

                    <div class="tray tray-center">

                    <!-- Validation Example -->
                    <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
                        <div class="panel">
                            <div class="panel-heading">
                              <span class="panel-title">Add Subscriber</span>
                            </div>
                            <div class="panel-body">
                                <form id="admin-form" class="form-horizontal" role="form" method="POST" action="{{ route('subscribers.store') }}" autocomplete="off">
                                <input type="hidden" name="id" value="{{ $edit_id ?? '' }}">
                                <input type="hidden" name="_method" value="{{ Route::currentRouteNamed('subscribers.create') ? 'POST' : 'PUT' }}">
                                {{ csrf_field() }}


                                <div class="form-group">
                                  <label for="email" class="col-lg-3 control-label">Email</label>

                                  <div class="col-lg-8">
                                    <label for="email" class="field {{ $errors->has('email') ? 'state-error' : ''}}">
                                        <input type="text" value="{{ !is_null(old('email')) ? old('email') : (isset($data) ? $data->email : '') }}" name="subscriber_email" id="email" class="gui-input" placeholder="Email">
                                    </label>
                                      @if($errors->has('subscriber_email'))
                                        <em for="email" class="state-error text-danger">{{ $errors->first('subscriber_email') }}</em>
                                      @endif
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="fuel_type_id" class="col-lg-3 control-label">Choose Player</label>

                                  <div class="col-lg-8">
                                    <label for="player_espn_id" class="field {{ $errors->has('player_espn_id') ? 'state-error' : ''}}">
                                        <select  class="selectpicker w-100" name="player_espn_ids[]" data-live-search="true" multiple>
                                          @foreach($players as $key => $player)
                                              <option value="{{ $player->espn_id }}" {{ (!is_null(old('player_espn_id')) && isset($data) && $key == old('player_espn_id')) ? 'selected' : ((isset($data) && $player->player_espn_id == $key ) ? 'selected'  : '') }} >{{ $player->first_name.' '.$player->last_name }}</option>
                                           @endforeach
                                        </select>
                                    </label>

                                      @if($errors->has('player_espn_ids'))
                                        <em for="fuel_type_id" class="state-error text-danger">{{ $errors->first('player_espn_ids') }}</em>
                                      @endif
                                  </div>
                                </div>
                                <div class=" text-right">
                                    <button type="submit" class="button btn-primary">Add Subscriber</button>
                                    <a href="{{ url ('/admin/subscribers') }}" type="reset" class="button"> Cancel </a>
                                </div>
                              </form>
                            </div>
                        </div>
                    </div>
                    <!-- end: .admin-form -->
                </div>
                <!-- Begin: Admin Form -->
            </div>
        </div>
        <!-- end: .tray-center -->
</section>
@push('footer-scripts')
  <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
@endpush
@endsection
